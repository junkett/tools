

SECRET_NAME=$1
SECRET_PATH=$2
aws secretsmanager create-secret --name $SECRET_NAME 2>/dev/null
if [[ ($? -eq 0) || ($? -eq 255) ]]
then 
    aws secretsmanager update-secret --secret-id $SECRET_NAME --secret-string "$(cat $SECRET_PATH)"
else 
    exit 1 
fi


