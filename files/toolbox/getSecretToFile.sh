

SECRET_NAME=$1
SECRET_PATH=$2
aws secretsmanager get-secret-value --secret-id $SECRET_NAME | jq -r  .SecretString | tr "\\n" "\n" > $SECRET_PATH


