cd /root
echo "$SSH_KEY" | sed -e 's/\\n/\n/g' > /root/.ssh/id_rsa
echo "$SSH_KEY_AWS" | sed -e 's/\\n/\n/g' > /root/.ssh/aws
sed -i "s/XKEY/$AWS_KEY/" /root/.aws/credentials
sed -i "s/XSECRET/$AWS_SEC/" /root/.aws/credentials
chmod 600 /root/.ssh/id_rsa
chmod 600 /root/.ssh/aws
git clone git@gitlab.com:junkett/ansible.git
git clone git@gitlab.com:junkett/terraform.git
cd /root/terraform
terraform init
cd /root
eval `ssh-agent -s` && ssh-add -k