FROM centos:latest

RUN yum update -y
RUN	yum clean all
RUN	yum install -y nano curl wget git unzip rsync gcc which iptables yum-utils bind-utils epel-release
RUN dnf install python3-paramiko -y
RUN dnf install python3 -y
RUN	yum clean all
RUN yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
RUN yum groupinstall 'development tools' -y
RUN yum install jq libunwind libicu terraform awscli -y

RUN echo 'export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$PATH' >> ~/.bashrc

# enable networking
RUN echo "NETWORKING=yes" >> /etc/sysconfig/network
ENV container docker

EXPOSE 22

COPY prerequirements.txt /
COPY requirements.txt /

RUN pip3 install -r /prerequirements.txt && \
  pip3 install -r /requirements.txt

RUN cd /tmp; wget https://git.io/get-mo; chmod +x get-mo; mv get-mo /usr/bin/mo

RUN mkdir -p /root/.ssh /root/.aws /root/work
RUN chmod 700 /root/.aws
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
COPY files/aws-config /root/.aws/config
COPY files/aws-credential /root/.aws/credentials
COPY files/ssh-config /root/.ssh/config
COPY files/toolbox /root/toolbox
RUN chmod 777 -R /root/toolbox
RUN chmod 700 /root/.ssh
RUN chmod 600 /root/.ssh/*
ENV KUBECONFIG /root/ansible/inventory/artifacts/k0s-kubeconfig.yml
